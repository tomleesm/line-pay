composer global require "laravel/installer"
echo 'export PATH=/root/.composer/vendor/bin:$PATH' >> ~/.bash_profile
source ~/.bash_profile
cd /var/www/html/apps/
composer create-project laravel/laravel=6.* demo --prefer-dist
cd demo
composer install
cd /var/www/html/apps
chown -R www-data:www-data pay-demo
